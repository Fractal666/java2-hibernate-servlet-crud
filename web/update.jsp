<%-- 
    Document   : update
    Created on : Oct 8, 2018, 1:51:27 PM
    Author     : Fractal
--%>

<%@page import="java.util.Date"%>
<%@page import="lt.bit.db.People"%>
<%@page import="java.math.BigDecimal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% People per = (People) request.getAttribute("objektas");%>
        <%
            String linkas;
            String a;
            String b;
            Date c;
            BigDecimal d;
            int value;

            if (request.getParameter("update") == "" || request.getParameter("update") == null) {
                linkas = "Addnew";
                a = "";
                b = "";
                c = null;
                d = null;
                value = -1;
            } else {
                linkas = "Update";
                // request.getParameter("id");
                a = per.getFirstName();
                b = per.getLastName();
                c = per.getBirthDate();
                d = per.getSalary();
                value = per.getId();

            };


        %>
        <form action="<%=linkas%>"method="POST"> 
            <input  placeholder="Name" name="name" value=<%= a%> >
            <input  placeholder="Surname" name="surname" value=<%= b%>>
            <input   placeholder="BirthDate" name="birthDate" value=<%= c == null ? "" : c%>>    
            <input  placeholder="Salary" name="salary" value=<%= d == null ? "" : d%>>
            <button type="submit" name="id" value="<%= value%>">Save</button>
            <a href="/Suhibernate">Cancel</a>
        </form>
    </body>
</html>
