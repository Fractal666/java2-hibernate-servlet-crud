<%-- 
    Document   : editContacts
    Created on : Oct 8, 2018, 1:50:01 PM
    Author     : Fractal
--%>

<%@page import="lt.bit.db.Contacts"%>
<%@page import="lt.bit.db.People"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% Contacts per = (Contacts) request.getAttribute("kontaktas");
            People pers = (People) request.getAttribute("zmId");


        %>
        <%            String linkas;
            String ctype;
            String contact;

            int value;
            Integer zmogausId;
            if (request.getParameter("add") != null) {
                zmogausId = Integer.parseInt(request.getParameter("add"));
                linkas = "Addnewcontact";
                ctype = "";
                contact = "";

                value = Integer.parseInt(request.getParameter("add"));
            } else {
                zmogausId = pers.getId();
                linkas = "Updatecontact";

                ctype = per.getCType();
                contact = per.getContact();
                value = Integer.parseInt(request.getParameter("update"));
            };


        %>
        <form action="<%=linkas%>"method="POST"> 
            <input  placeholder="C-type" name="ctype" value=<%= ctype%> >
            <input  placeholder="Contact" name="contact" value=<%= contact%>>

            <button type="submit" name="id" value="<%= value%>">Save</button>
            <a href="Cancelkontaktai?id=<%=zmogausId%>">Cancel</a>
        </form>
    </body>
</html>
