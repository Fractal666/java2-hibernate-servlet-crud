<%-- 
    Document   : address
    Created on : Oct 5, 2018, 3:50:30 PM
    Author     : Fractal
--%>

<%@page import="lt.bit.db.People"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <% People zmogus = (People) request.getAttribute("objektas1");%>
         <div class="container">
            <br>
            <div class="d-flex justify-content-inline">
            <form action="Pagrindinis" method="POST"> <button class="btn bg-info mr-1" name="add" type="submit" value="">Back to people list</button></form>
            <form action="editAddress.jsp" method="POST"> <button class="btn bg-info" name="add" type="submit" value="<%=zmogus.getId()%>">Add new</button></form>
            </div>
            <br>
            <table class="table">
                <thead>
                <th>Id</th>
                <th>Address</th>
                <th>City</th>
                <th>Postal code</th>
                <th></th>
                <th></th>
    
                    <% for (int i = 0; i <zmogus.getAddressList().size() ; i++) {%>
                <tr>
                   <td> <%= zmogus.getAddressList().get(i).getId()%></td>
                    <td> <%= zmogus.getAddressList().get(i).getAddress()%></td>
                    <td> <%=zmogus.getAddressList().get(i).getCity()%></td>
                    <td> <%=zmogus.getAddressList().get(i).getPc()%></td>
                    
                    <td><form action="Tarpinisadresai" method="POST"> <button class="btn bg-info" name="update" type="submit" value="<%=zmogus.getAddressList().get(i).getId()%>">Update</button></form></td>
                    <td><form action="Deleteadresai" method="POST"> <button class="btn bg-info" name="delete" type="submit" value="<%=zmogus.getAddressList().get(i).getId()%>">Delete</button></form></td>

                </tr>
                <%}%>
                </thead>
            </table>
        </div>
    </body>
</html>
