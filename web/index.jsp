<%-- 
    Document   : index
    Created on : Oct 3, 2018, 1:16:52 PM
    Author     : Fractal
--%>


<%@page import="lt.bit.db.People"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
   <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
   </head>
   <body>

        <% List<People> info = (List<People>) request.getAttribute("INFO");%>
        <div class="container">
            <br>
            <form action="update.jsp" method="POST"> <button class="btn bg-info" name="update" type="submit" value="">Add new</button></form>
            <br>
            <table class="table">
                <thead>
                <th>Id</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Birth_date</th>
                <th>Salary</th>
                <th>Address</th>
                <th>Contacts</th>
                <th></th>
                <th></th>
                    <% for (int i = 0; i < info.size(); i++) {%>
                <tr>
                    <td> <%= info.get(i).getId()%></td>
                    <td> <%= info.get(i).getFirstName()%></td>
                    <td> <%= info.get(i).getLastName()%></td>
                    <td> <%= info.get(i).getBirthDate()%></td>
                    <td> <%= info.get(i).getSalary()%></td>
                    <td> <a href="adresuinfo?id=<%= info.get(i).getId()%>">Address</a></td>
                    <td> <a href="kontaktuinfo?id=<%= info.get(i).getId()%>">Contacts</a></td>
                    <td><form action="Tarpinis" method="POST"> <button class="btn bg-info" name="update" type="submit" value="<%=info.get(i).getId()%>">Update</button></form></td>
                    <td><form action="Delete" method="POST"> <button class="btn bg-info" name="delete" type="submit" value="<%=info.get(i).getId()%>">Delete</button></form></td>

                </tr>
                <% } %>
                </thead>
            </table>
        </div>
    </body>
</html>
