<%-- 
    Document   : people
    Created on : Oct 5, 2018, 3:50:59 PM
    Author     : Fractal
--%>

<%@page import="lt.bit.db.People"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="lt.bit.db.Address"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% Address per = (Address) request.getAttribute("adresas");
            People pers = (People) request.getAttribute("zmId");
            Integer zmogausId;

        %>
        <%            String linkas;
            String address;
            String city;
            String pc;

            int value;

            if (request.getParameter("add") != null) {
                zmogausId = Integer.parseInt(request.getParameter("add"));
                linkas = "Addnewaddress";
                address = "";
                city = "";
                pc = "";

                value = Integer.parseInt(request.getParameter("add"));
            } else {

                zmogausId = pers.getId();

                linkas = "Updateaddress";

                address = per.getAddress();
                city = per.getCity();
                pc = per.getPc();
                value = Integer.parseInt(request.getParameter("update"));
            };


        %>
        <form action="<%=linkas%>"method="POST"> 
            <input  placeholder="Address" name="address" value=<%= address%> >
            <input  placeholder="City" name="city" value=<%= city%>>
            <input  placeholder="Postal code" name="pc" value=<%= pc%>>

            <button type="submit" name="id" value="<%= value%>">Save</button>
            <a href="Canceladresai?id=<%=zmogausId%>">Cancel</a>
        </form>
    </body>
</html>
